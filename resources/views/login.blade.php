<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Inicio de Sesion - Registro</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Place favicon.ico in the root directory -->
	@include('layouts.rels')

</head>
<body>
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<div class="wrapper home-3">
		<!--Header Area Start-->
		@include('layouts.header')
		<!--Header Area End-->

		<section class="heading-banner-area pt-30">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="heading-banner">
		                    <div class="breadcrumbs">
		                        <ul>
		                            <li><a href="#">Inicio</a><span class="breadcome-separator">></span></li>
		                            <li>Login</li>
		                        </ul>
		                    </div>
		                    <div class="heading-banner-title">
		                        <h1>Login</h1>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>

		<!--My Account Area Start-->
		<section class="my-account-area mt-20 pt-20">
		    <div class="container">
		        <div class="row">
                    <!--Login Form Start-->
		            <div class="col-lg-6 col-md-6">
		                <div class="customer-login-register">
		                    <div class="form-login-title">
		                        <h2>Inicio de Sesión</h2>
		                    </div>
		                    <div class="login-form">
		                        <form action="{{route('loginproceso')}}" method="POST">
                                    @csrf
		                            <div class="form-fild">
		                                <p><label>Correo<span class="required">*</span></label></p>
		                                <input type="text" name="username" value="" >
		                            </div>
		                            <div class="form-fild">
		                                <p><label>Clave <span class="required">*</span></label></p>
		                                <input type="password" name="password" value="" >
		                            </div>
		                            <div class="login-submit">
		                                <button type="submit" class="form-button">Entrar</button>


		                            </div>

									<div class="login-submit pt-20">
										<h3>O inicia sesión con:</h3>

		                                <a href="login/facebook" class="form-button bg-primary">Facebook</a>
										<a href="login/google" class="form-button bg-danger">Google</a>
		                            </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		            <!--Login Form End-->
		            <!--Register Form Start-->
		            <div class="col-lg-6 col-md-6">
		                <div class="customer-login-register register-pt-0">
		                    <div class="form-register-title">
		                        <h2>Registro</h2>
		                    </div>
		                    <div class="register-form">
		                        <form action="{{route('registro')}}" method="POST">
                                    @csrf
		                            <div class="form-fild">
		                                <p><label>Correo<span class="required">*</span></label></p>
		                                <input type="text" name="username" value="" >
		                            </div>
		                            <div class="form-fild">
		                                <p><label>Clave <span class="required">*</span></label></p>
		                                <input type="password" name="password" value="" >
		                            </div>
		                            <div class="register-submit">
		                                <button type="submit" class="form-button">Registrar</button>
		                            </div>
		                        </form>
		                    </div>
		                </div>
		            </div>
		            <!--Register Form End-->
		        </div>
		    </div>
		</section>
		<!--My Account Area End-->

		<!--Footer Area Start-->
		@include('layouts.footer')
		<!--Footer Area End-->
        <!--Modal Start-->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal Content Strat-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="modal-details">
                   <div class="row">
                       <!--Product Img Strat-->
                       <div class="col-xl-5 col-lg-5">
                           <!--Product Tab Content Start-->
                            <div class="tab-content">
                                <div id="watch1" class="tab-pane fade show active">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/1.jpg" alt="">
                                    </div>
                                </div>
                                <div id="watch2" class="tab-pane fade">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/2.jpg" alt="">
                                    </div>
                                </div>
                                <div id="watch3" class="tab-pane fade">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/3.jpg" alt="">
                                    </div>
                                </div>
                                <div id="watch4" class="tab-pane fade">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/4.jpg" alt="">
                                    </div>
                                </div>
                                <div id="watch5" class="tab-pane fade">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/5.jpg" alt="">
                                    </div>
                                </div>
                                <div id="watch6" class="tab-pane fade">
                                    <div class="modal-img img-full">
                                        <img src="img/single-product/large/6.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                            <!--Product Tab Content End-->
		                    <!--Single Product Tab Menu Start-->
		                    <div class="modal-product-tab">
		                        <ul class="nav">
		                            <li><a class="active" data-toggle="tab" href="#watch1"><img src="img/product-thumb/1.jpg" alt=""></a></li>
		                            <li><a data-toggle="tab" href="#watch2"><img src="img/product-thumb/3.jpg" alt=""></a></li>
		                            <li><a data-toggle="tab" href="#watch3"><img src="img/product-thumb/2.jpg" alt=""></a></li>
		                            <li><a data-toggle="tab" href="#watch4"><img src="img/product-thumb/4.jpg" alt=""></a></li>
		                        </ul>
		                    </div>
		                    <!--Single Product Tab Menu Start-->
                       </div>
                       <!--Product Img End-->
                       <!-- Product Content Start-->
                       <div class="col-xl-7 col-lg-7">
                           <div class="product-info">
                               <h2>Natural passages</h2>
                               <div class="product-price">
                                   <span class="old-price">$74.00</span>
                                   <span class="new-price">$69.00</span>
                               </div>
                               <a href="#" class="see-all">See all features</a>
                               <div class="add-to-cart quantity">
                                    <form class="add-quantity" action="#">
                                         <div class="quantity modal-quantity">
                                             <label>Quantity</label>
                                             <input type="number">
                                         </div>
                                        <div class="add-to-link">
                                            <button class="form-button" data-text="add to cart">add to cart</button>
                                        </div>
                                    </form>
                               </div>
                               <div class="cart-description">
                                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus.</p>
                               </div>
                               <div class="social-share">
                                   <h3>Share this product</h3>
                                   <ul class="socil-icon2">
                                       <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                       <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                       <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                                       <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                       <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                   </ul>
                               </div>
                           </div>
                       </div>
                       <!--Product Content End-->
                   </div>
                </div>
              </div>
            </div>
            <!--Modal Content Strat-->
          </div>
        </div>
        <!--Modal End-->
	</div>



    <!--All Js Here-->

	<!--Jquery 3.6.0-->
	<script src="js/vendor/modernizr-3.6.0.min.js"></script>
	<script src="js/vendor/jquery-3.6.0.min.js"></script>
	<script src="js/vendor/jquery-migrate-3.3.2.min.js"></script>
	<!--Popper-->
	<script src="js/popper.min.js"></script>
	<!--Bootstrap-->
	<script src="js/bootstrap.min.js"></script>
	<!--Imagesloaded-->
	<script src="js/imagesloaded.pkgd.min.js"></script>
	<!--Isotope-->
	<script src="js/isotope.pkgd.min.js"></script>
	<!--Ui js-->
	<script src="js/jquery-ui.min.js"></script>
	<!--Countdown-->
	<script src="js/jquery.countdown.min.js"></script>
	<!--Counterup-->
	<script src="js/jquery.counterup.min.js"></script>
	<!--ScrollUp-->
	<script src="js/jquery.scrollUp.min.js"></script>
	<!--Chosen js-->
	<script src="js/chosen.jquery.js"></script>
	<!--Meanmenu js-->
	<script src="js/jquery.meanmenu.min.js"></script>
	<!--Instafeed-->
	<script src="js/instafeed.min.js"></script>
	<!--EasyZoom-->
	<script src="js/easyzoom.min.js"></script>
	<!--Fancybox-->
	<script src="js/jquery.fancybox.pack.js"></script>
	<!--Nivo Slider-->
	<script src="js/jquery.nivo.slider.js"></script>
	<!--Waypoints-->
	<script src="js/waypoints.min.js"></script>
	<!--Carousel-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Slick-->
	<script src="js/slick.min.js"></script>
	<!--Wow-->
	<script src="js/wow.min.js"></script>
	<!--Plugins-->
	<script src="js/plugins.js"></script>
	<!--Main Js-->
	<script src="js/main.js"></script>
</body>
</html>
