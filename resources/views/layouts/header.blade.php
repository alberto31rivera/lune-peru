<header>
    <div class="header-container">
        <!--Header Top Area Start-->
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <!--Header Top Left Area Start-->
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="header-top-menu">
                            <ul>
                                <li><span>Síguenos:</span><a href="#"> <i class="fa fa-angle-down"></i></a>
                                    <ul class="ht-dropdown">
                                        <li><a target="_blank" href="https://www.facebook.com/LuneySoleil1">Facebook</a></li>
                                        <li><a target="_blank" href="https://www.instagram.com/luneysoleilperu/">Instagram</a></li>
                                        <li><a target="_blank" href="https://www.tiktok.com/@luneysoleil">TikTok</a></li>
                                    </ul>
                                </li>
                                <li><span>Whatsapp:</span><a href="#" class="text-uppercase"><i
                                            class="fa fa-angle-down"></i></a>
                                    <ul class="ht-dropdown">
                                        <li><a target="_blank" href="https://wa.link/g9taey">Asesora Venta 1</a></li>
                                        <li><a target="_blank" href="https://wa.link/ahfv0k">Asesora 2 </a></li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--Header Top Left Area End-->
                    <!--Header Top Right Area Start-->
                    <div class="col-lg-8 col-md-8 d-lg-block d-md-block d-none text-right">
                        <div class="header-top-menu">
                            <ul>
                                <li class="support"><span>Para entrega rápida, ordene antes de las 5:30 pm </span></li>
                                <li class="account"><a href="#">Mi Cuenta <i class="fa fa-angle-down"></i></a>
                                    @if(Auth::user() != null)
                                    <ul class="ht-dropdown">
                                        <li><a href="{{route('micuenta')}}">Cuenta</a></li>
                                        <li><a href="{{route('logout')}}">Cerrar Sesión</a></li>
                                    </ul>
                                    @else
                                    <ul class="ht-dropdown">
                                        <li><a href="{{route('login_post')}}">Inicia Sesión</a></li>
                                    </ul>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--Header Top Right Area End-->
                </div>
            </div>
        </div>
        <!--Header Top Area End-->
        <!--Header Middel Area Start-->
        <div class="header-middel-area">
            <div class="container">
                <div class="row">
                    <!--Logo Start-->
                    <div class="col-lg-3 col-md-3 col-12">
                        <div class="logo">
                            <a href="/"><img src="{{ asset('img/logo/logo2.png') }}" alt="Logo {{ config('app.name') }}"></a>
                        </div>
                    </div>
                    <!--Logo End-->
                    <!--Search Box Start-->
                    <div class="col-lg-6 col-md-5 col-12">
                        <div class="search-box-area">
                                    <form action="{{route('buscar')}}">
                                        <div class="select-area">
                                            <select  style="border:0!important;" data-placeholder="Choose a Country..." class="select" tabindex="1">
                                                <option value="">Buscar</option>

                                                <!-- <optgroup label="AFC SOUTH">
                                                  <option>Houston Texans</option>
                                                  <option>Indianapolis Colts</option>
                                                  <option>Jacksonville Jaguars</option>
                                                  <option>Tennessee Titans</option>
                                                </optgroup>
                                                <optgroup label="AFC WEST">
                                                  <option>Denver Broncos</option>
                                                  <option>Kansas City Chiefs</option>
                                                  <option>Oakland Raiders</option>
                                                  <option>San Diego Chargers</option>
                                                </optgroup> -->
                                           </select>
                                        </div>
                                        <div class="search-box">
                                            <input type="text" name="q" id="search" placeholder="¡Qué buscas?" onblur="if(this.value==''){this.value='Search product...'}" onfocus="if(this.value=='Search product...'){this.value=''}">
                                            <button type="submit"><i class="ion-ios-search-strong"></i></button>
                                        </div>
                                    </form>
                                </div>
                    </div>
                    <!--Search Box End-->
                    <!--Mini Cart Start-->
                    <div class="col-lg-3 col-md-4 col-12">
                        <div class="mini-cart-area">
                            <ul>
                                @if($pedido != null)
                                <li><a href="#"><i class="ion-android-cart"></i><span
                                            class="cart-add">{{$cantidad}}</span><span class="cart-total">S/ {{$pedido->total}}
                                            <i class="fa fa-angle-down"></i></span></a>
                                    <ul class="cart-dropdown">
                                        @foreach ($pedido->relacionpedido as $producto)
                                            <!--Single Cart Item Start-->
                                        <li class="cart-item">
                                            <div class="cart-content">
                                                <h4><a href="shopping-cart.html">{{$producto->detalle->titulo}}</a></h4>
                                                <p class="cart-quantity">Cant: {{$producto->cantidad}}</p>
                                                <p class="cart-price">S/ {{$producto->cantidad * $producto->detalle->precio}}</p>
                                            </div>
                                        </li>
                                        <!--Single Cart Item Start-->
                                        @endforeach
                                        <!--Cart Total Start-->
                                        <li class="cart-total-amount mtb-20">
                                            <h4>SubTotal : <span class="pull-right">S/ {{$pedido->total}}</span></h4>
                                        </li>
                                        <!--Cart Total End-->
                                        <!--Cart Button Start-->
                                        <li class="cart-button">
                                            <a href="{{route('carrito')}}" class="button2">Ver Carrito</a>
                                            <a href="{{route('checkout', $pedido->id)}}" class="button2">Pagar</a>
                                        </li>
                                        <!--Cart Button End-->
                                    </ul>
                                </li>
                                @else
                                <li><a href="#"><i class="ion-android-cart"></i><span
                                            class="cart-add">0</span><span class="cart-total">S/ 00,00
                                            <i class="fa fa-angle-down"></i></span></a>
                                    <ul class="cart-dropdown">
                                        No hay Productos en tu carrito
                                    </ul>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!--Mini Cart End-->
                </div>
            </div>
        </div>
        <!--Header Middel Area End-->
        <!--Header bottom Area End-->

        <!--Header bottom Area Start-->
        <div class="header-bottom-area header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                                <div class="side-menu">
                                    <div class="category-heading">
                                        <h2><i class="ion-android-menu"></i><span><a href="{{route('tienda')}}">Ofertas</a></span></h2>
                                    </div>
                                   <!--  <div id="cate-toggle" class="category-menu-list">
                                        <ul>
                                            <li class="right-menu"><a href="shop.html">Electronic</a>
                                                <ul class="cat-mega-menu" style="">
                                                    <li class="right-menu cat-mega-title">
                                                       <a href="shop.html">Television</a>
                                                        <ul style="">
                                                            <li><a href="shop.html">LCD TV</a></li>
                                                            <li><a href="shop.html">LED TV</a></li>
                                                            <li><a href="shop.html">Curved TV</a></li>
                                                            <li><a href="shop.html">Plasma TV</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="right-menu cat-mega-title">
                                                       <a href="shop.html">Refrigerator</a>
                                                        <ul style="">
                                                            <li><a href="shop.html">Brand1</a></li>
                                                            <li><a href="shop.html">Brand2</a></li>
                                                            <li><a href="shop.html">Brand3</a></li>
                                                            <li><a href="shop.html">Brand4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="right-menu cat-mega-title">
                                                       <a href="shop.html">Smart Phone</a>
                                                        <ul style="">
                                                            <li><a href="shop.html">Brand2</a></li>
                                                            <li><a href="shop.html">Brand4</a></li>
                                                            <li><a href="shop.html">Brand6</a></li>
                                                            <li><a href="shop.html">Brand3</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Fashion &amp; Beauty</a></li>
                                            <li class="right-menu"><a href="shop.html">Camera &amp; Photo</a>

                                                <ul class="cat-dropdown" style="">
                                                    <li><a href="shop.html">Brand6</a></li>
                                                    <li><a href="shop.html">Brand2</a></li>
                                                    <li><a href="shop.html">Brand1</a></li>
                                                    <li><a href="shop.html">Brand7</a></li>
                                                </ul>

                                            </li>
                                            <li class="right-menu"><a href="#">Smart Phone &amp; Tablet</a>

                                                <ul class="cat-dropdown" style="">
                                                    <li><a href="shop.html">Brand6</a></li>
                                                    <li><a href="shop.html">Brand2</a></li>
                                                    <li><a href="shop.html">Brand1</a></li>
                                                    <li><a href="shop.html">Brand7</a></li>
                                                </ul>

                                            </li>
                                            <li><a href="shop.html">Sport &amp; Outdoor</a></li>
                                            <li><a href="shop.html">Automotive &amp; Morocycle</a></li>
                                            <li><a href="shop.html">Washing machine</a></li>
                                            <li><a href="shop.html">Brand3</a></li>
                                            <li><a href="shop.html">Brand2</a></li>
                                            <li class="rx-child"><a href="shop.html">Sanyo</a></li>
                                            <li class="rx-child"><a href="shop.html">Brand1</a></li>
                                            <li class="rx-child"><a href="shop.html">Jewelry &amp; Watches</a></li>
                                            <li class="rx-child"><a href="shop.html">Holiday Supplies &amp; Gifts</a></li>
                                            <li class="rx-child"><a href="shop.html">Toys &amp; Hobbies</a></li>
                                            <li class="rx-parent">
                                                <a class="rx-default"><span class="cat-thumb  fa fa-ellipsis-h"></span>More</a>
                                                <a class="rx-show"><span class="cat-thumb  fa fa-ellipsis-h"></span>Less</a>
                                            </li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>
                    <!--Main Menu Start-->
                    <div class="col-lg-9 col-md-9">
                        <!--Logo Sticky Start-->
                        <div class="logo-sticky">
                            <a href="/">
                                <img src="{{asset('img/logo/logo2.png')}}" alt="Logo {{ config('app.name') }}">
                            </a>
                        </div>
                        <!--Logo Sticky Start-->
                        <!--Main Menu Area Start-->
                        <div class="main-menu-area">
                            <nav>
                                <ul class="main-menu">
                                    <li class="active"><a href="/">Inicio </a>

                                    </li>

                                    <li class="hot"><a href="{{route('tienda')}}">Tienda</a></li>
                                    <li><a href="{{route('nosotros')}}">Nosotros</a></li>
                                    <li><a href="{{route('contacto')}}">Contacto</a></li>

                                </ul>
                            </nav>
                        </div>
                        <!--Main Menu Area End-->
                    </div>
                    <!--Main Menu Start-->
                </div>
            </div>
        </div>
    </div>
</header>

<script>
    function buscar() {
        q = $("#q").val();
        url = "/buscar/" + q;

        $(location).attr('href', url);
    }
</script>
