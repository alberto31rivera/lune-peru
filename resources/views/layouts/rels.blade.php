<link rel="shortcut icon" type="image/x-icon" href="{{ route('welcome') }}/public/img/favicon.ico">

    <!-- Ionicons Font CSS-->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- font awesome CSS-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- UI CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- Chosen CSS-->
    <link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
    <!-- Meanmenu CSS-->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
    <!-- Normalize CSS-->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- Nivo Slider CSS-->
    <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- EasyZoom CSS-->
    <link rel="stylesheet" href="{{ asset('css/easyzoom.css') }}">
    <!-- Slick CSS-->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ env('URL_default') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ env('URL_STYLES') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.0/flexslider.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
<script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
