<?php $total_pedido = 0; ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Videos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <section class="heading-banner-area pt-30">
		    <div class="container">
		        <div class="row">
		            <div class="col-lg-12">
		                <div class="heading-banner">
		                    <div class="breadcrumbs">
		                        <ul>
		                            <li><a href="index.html">Inicio</a><span class="breadcome-separator">></span></li>
		                            <li>Videos</li>
		                        </ul>
		                    </div>
		                    <div class="heading-banner-title">
		                        <h1>Videos</h1>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>


        <section class="about-us-area">

            <div class="about-us-img bg-4"></div>

		    <div class="container-fluid">
		        <div class="row">

                    <div class="col-lg-6 offset-lg-6 col-md-6 offset-md-6 about-us-content">
                        <div class="about-us-title text-center">
                            <h2><strong>VIDEO </strong><strong>MODA 2022</strong></h2>
                        </div>
                       
                            <div class="post-video embed-responsive embed-responsive-16by9">
                                <video src="videos/video1.mp4" width="640" height="480" controls></video>
                            </div>
                        

                    </div>

		        </div>
		    </div>
		</section>

        <section class="counter-up-area">
            <div class="row no-gutters">
                <!--Single Count Box Start-->
                
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12 p-3">
                    <div class="blog-post-wrapper">
                        <div class="post-thumbnail img-full">
                            <div class="post-video embed-responsive embed-responsive-16by9">
                                <video src="public/videos/video1.mp4" width="640" height="480" controls></video>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--Single Count Box End-->
                <!--Single Count Box Start-->
                <div class="col-lg-3 col-md-6 col-12 p-3">
                    <div class="blog-post-wrapper">
                        <div class="post-thumbnail img-full">
                            <div class="post-video embed-responsive embed-responsive-16by9">
                                <video src="public/videos/video2.mp4" width="640" height="480" controls></video>
                            </div>
                        </div>
                        
                    </div>
                </div>
              
            </div>
		</section>

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
</body>

</html>
