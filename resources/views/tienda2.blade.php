<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tienda</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

</head>

<body>
    <!--[if lt IE 8]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->
        <!--Product List Grid View Area Start-->
        <div class="product-list-grid-view-area mt-20">
            <div class="container">
                <div class="row">
                    <!--Shop Product Area Start-->
                    <div class="col-lg-9 order-lg-2 order-1">
                        <div class="shop-desc-container">
                            <div class="row">
                                <!--Shop Product Image Start-->
                                <div class="col-lg-12">
                                    <div class="shop-product-img mb-30 img-full">
                                        <img src="img/offer/11.jpg" alt="">
                                    </div>
                                </div>
                                <!--Shop Product Image Start-->
                            </div>
                        </div>
                        <!--Shop Tab Menu Start-->
                        <div class="shop-tab-menu">
                            <div class="row">
                                <!--List & Grid View Menu Start-->
                                <div class="col-lg-5 col-md-5 col-xl-6 col-12">
                                    <div class="shop-tab">
                                        <ul class="nav">
                                            <li><a class="active" data-toggle="tab" href="#grid-view"><i
                                                        class="ion-android-apps"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                                <!--List & Grid View Menu End-->
                                <!-- View Mode Start-->

                                <!-- View Mode End-->
                            </div>
                        </div>
                        <!--Shop Tab Menu End-->
                        <!--Shop Product Area Start-->
                        <div class="shop-product-area">
                            <div class="tab-content">
                                <!--Grid View Start-->
                                <div id="grid-view" class="tab-pane fade show active">
                                    <div class="row product-container">
                                        @foreach ($productos as $producto)
                                            <!--Single Product Start-->
                                            <div class="col-lg-3 col-md-3 item-col2">
                                                <div class="single-product">
                                                    <div class="product-img">
                                                        <a href="/producto/{{strtolower(str_replace(" ", "-", $producto->titulo))}}">

                                                            <img class="first-img" src="https://imagenes.luneperu.com/{{$producto->imagenes->imagen}}" alt="">
                                                        </a>

                                                    </div>
                                                    <div class="product-content">
                                                        <h2><a href="/producto/{{strtolower(str_replace(" ", "-", $producto->titulo))}}">{{$producto->titulo}}</a></h2>

                                                        <div class="product-price">
                                                            <span class="new-price">S/ {{$producto->precio}}</span>
                                                            <a class="button add-btn" href="/producto/{{strtolower(str_replace(" ", "-", $producto->titulo))}}" data-toggle="tooltip"
                                                                title="Add to Cart">Agregar al Carrito</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Single Product End-->
                                        @endforeach

                                    </div>

                                </div>
                                <!--Grid View End-->

                            </div>
                        </div>
                        <!--Shop Product Area End-->
                        <!--Pagination Start-->
                        <div class="pagination pb-10">

                        </div>
                        <!--Pagination End-->
                    </div>
                    <!--Shop Product Area End-->
                    <!--Left Sidebar Start-->
                    <div class="col-lg-3 order-lg-1 order-2">
                        <!--Widget Price Slider Start-->
                        <div class="widget widget-price-slider">
                            <h3 class="widget-title">Filtro por Precio</h3>
                            <div class="widget-content">
                                <div class="price-filter">
                                    <form action="{{route('filtrar')}}" method="get">
                                        @csrf
                                        <div id="slider-range"></div>
                                        <span>Precio:<input id="amount" name="monto" class="amount" readonly=""
                                                type="text"></span>
                                        <input class="price-button" value="Filtrar" type="submit">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--Widget Price Slider End-->

                        @include('layouts.publicidad')
 

                         

                    </div>
                    <!--Left Sidebar End-->
                </div>
            </div>
        </div>
        <!--Product List Grid View Area End-->

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->

    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.0/jquery.flexslider.js') }}"></script>
</body>

</html>
