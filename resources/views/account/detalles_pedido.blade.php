<?php $total_pedido = 0; ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Carrito de Compra</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- Ionicons Font CSS-->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- font awesome CSS-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- UI CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- Chosen CSS-->
    <link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
    <!-- Meanmenu CSS-->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
    <!-- Normalize CSS-->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- Nivo Slider CSS-->
    <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- EasyZoom CSS-->
    <link rel="stylesheet" href="{{ asset('css/easyzoom.css') }}">
    <!-- Slick CSS-->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="stylesheet" href="{{ asset('flexslider.css') }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <!--Shopping Cart Area Start-->
        <div class="shopping-cart-area mt-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif

                            @if ($message = Session::get('danger'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif
                        <form class="shop-form" action="#">
                            <div class="wishlist-table table-responsive">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="product-cart-img">
                                                <span class="nobr"></span>
                                            </th>
                                            <th class="product-name">
                                                <span class="nobr">Titulo</span>
                                            </th>
                                            <th class="product-quantity">
                                                <span class="nobr">Cantidad</span>
                                            </th>
                                            <th class="product-price">
                                                <span class="nobr"> Precio </span>
                                            </th>
                                            <th class="product-total-price">
                                                <span class="nobr"> Total </span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedido->relacionpedido as $producto)
                                            <tr>
                                                <td class="product-cart-img">
                                                    <a href="#"><img
                                                            src="https://imagenes.luneperu.com/{{ $producto->imagenes->imagen }}"
                                                            width="50px" alt=""></a>
                                                </td>
                                                <td class="product-name">
                                                    <a href="#">{{ $producto->detalle->titulo }}</a> <br>
                                                    <span>TALLA: {{ $producto->variacion->talla_detalle->talla }}</span> <br>
                                                    <span>COLOR: {{ $producto->variacion->color_detalle->color }}</span><br>
                                                    <span>MATERIAL: {{ $producto->variacion->material_detalle->material }}</span>
                                                </td>
                                                <td class="quantity">
                                                   {{$producto->cantidad}}
                                                </td>
                                                <td class="product-price">
                                                    <span><ins>{{ $producto->detalle->precio }}</ins></span>
                                                </td>
                                                <td class="product-total-price">
                                                    <span>{{ $total_producto = $producto->detalle->precio * $producto->cantidad }}</span>

                                                    <?php $total_pedido = $total_pedido + $total_producto; ?>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <input type="hidden" value="{{ $total_pedido }}" id="subTotal">
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">

                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="shopping-cart-total">
                            <h2>Total del Carrito</h2>
                            <div class="shop-table table-responsive">
                                <table>
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <td data-title="Subtotal"><span>S/ {{ $total_pedido }}</span></td>
                                        </tr>
                                        @if ($cantidad > 2)
                                            <?php $descuento = ($total_pedido / 100) * $porcentaje; ?>
                                            <tr class="shipping">
                                                <td data-title="Descuento"><Span>S/ {{ $descuento }}</Span>
                                            </tr>
                                        @else
                                            <?php $descuento = 0; ?>
                                        @endif
                                        @if ($pedido->cupon != null)
                                            <tr class="shipping">
                                                <td data-title="Cupon">
                                                    ({{$pedido->cupon_detalle->codigo}})
                                                    <Span>S/
                                                        <?php
                                                        if ($pedido->cupon_detalle->tipo == 1) {
                                                            $desc = ($total_pedido / 100) * $pedido->cupon_detalle->aplica;
                                                            echo '- ' . number_format($desc, 0);
                                                        } elseif ($pedido->cupon_detalle->tipo == 2) {
                                                            echo $desc = $pedido->cupon_detalle->aplica;
                                                        } else {
                                                            $desc = 0;
                                                        }
                                                        ?>
                                                    </Span>
                                            </tr>
                                        @else
                                            <?php $desc = 0; ?>
                                        @endif
                                        <tr class="order-total">
                                            <td data-title="Total"><span><strong>S/
                                                        {{ $total_pedido = $total_pedido - $desc - $descuento }}</strong></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <input type="hidden" value="{{ $total_pedido }}" id="total_pedido">
                            <input type="hidden" value="{{ $pedido->id }}" id="idPedido">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Shopping Cart Area End-->


    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>


</body>

</html>
