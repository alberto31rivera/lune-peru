<?php $total_pedido = 0; ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Carrito de Compra</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- Ionicons Font CSS-->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- font awesome CSS-->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- UI CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
    <!-- Chosen CSS-->
    <link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
    <!-- Meanmenu CSS-->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- Fancybox CSS-->
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
    <!-- Normalize CSS-->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- Nivo Slider CSS-->
    <link rel="stylesheet" href="{{ asset('css/nivo-slider.css') }}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- EasyZoom CSS-->
    <link rel="stylesheet" href="{{ asset('css/easyzoom.css') }}">
    <!-- Slick CSS-->
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Default CSS -->
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link rel="stylesheet" href="{{ asset('flexslider.css') }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <!--Shopping Cart Area Start-->
        <div class="shopping-cart-area mt-20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="wishlist-table table-responsive row">
                            <div class="row">
                            <div class="col-md-3">
                                <table>
                                    <thead>
                                        <tr>
                                            <th class="product-name">
                                                Menu
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="{{route('micuenta')}}">Mis pedidos</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="{{route('cambiarclave')}}">Cambiar Contraseña</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="{{route('logout')}}">Cerrar Sesion</a>
                                            </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                            <div class="col-md-9">
                                <h3>Mis Pedidos</h3>
                                <table style="margin-bottom: 50px">
                                    <thead>
                                        <tr>
                                            <th class="product-name">
                                                <span class="nobr">Id</span>
                                            </th>
                                            <th class="product-name">
                                                <span class="nobr">Fecha</span>
                                            </th>
                                            <th class="product-quantity">
                                                <span class="nobr">Estado</span>
                                            </th>
                                            <th class="product-price">
                                                <span class="nobr"> Total </span>
                                            </th>
                                            <th class="product-total-price">
                                                <span class="nobr"> Ver </span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedidos as $pedido)
                                            <tr>
                                                <td class="product-name">
                                                    {{ $pedido->id }}
                                                </td>
                                                <td class="product-name">
                                                    {{ $pedido->created_at }}
                                                </td>
                                                <td class="quantity">
                                                    @if ($pedido->estado == 1)
                                                        <span class="badge badge-success">Pagado</span>
                                                    @elseif($pedido->estado == 2)
                                                        <span class="badge badge-warning">En Espera de Validacion</span>
                                                    @elseif($pedido->estado == 3)
                                                        <span class="badge badge-danger">Rechazado o Denegado</span>
                                                    @elseif($pedido->estado == 4)
                                                        <span class="badge badge-primary">Despachado</span>
                                                    @elseif($pedido->estado == 5)
                                                        <span class="badge badge-danger">Dinero Retornado</span>
                                                    @endif
                                                </td>
                                                <td class="product-price">
                                                    S/ {{ $pedido->total }}
                                                </td>
                                                <td class="product-total-price">
                                                    @if ($pedido->factura != null)
                                                        <a href="https://facturas.luneperu.com/{{$pedido->factura}}"><i
                                                            class="fa fa-file text-danger"
                                                            style="font-size: 25px"></i></a>
                                                    @endif
                                                    <a href="{{ route('detalle_pedido', $pedido->id) }}"><i
                                                            class="fa fa-eye text-success"
                                                            style="font-size: 25px"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <input type="hidden" value="{{ $total_pedido }}" id="subTotal">
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Shopping Cart Area End-->

    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>


</body>

</html>
