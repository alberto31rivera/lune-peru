@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Crear Producto
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <form action="{{ route('storeProducto') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Categorias</label>
                    <select name="categoria" id="categoria" disabled onchange="getSubCategorias()" class="form-control">
                        <option value="">MUJER</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Titulo</label>
                    <input type="text" name="titulo" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Marca</label>
                    <input type="text" name="marca" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">SKU</label>
                    <input type="text" name="sku" id="sku" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Precio</label>
                    <input type="text" name="precio" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Descripcion</label>
                    <input type="text" name="descripcion" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Stock</label>
                    <input type="text" name="stock" class="form-control">
                </div>

                <div class="col-md-6 mb-3 mt-3" style="margin-top: 20px;">
                    <div class="card">
                        <div class="card-header">Seleccione los Colores</div>
                        <div class="card-body">
                            <select id='pre-selected-options' class="form-control" name="colores[]" multiple='multiple'>
                                @foreach ($colores as $color)
                                    <option value="{{ $color->id }}">{{ $color->color }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 mb-3 mt-3"  style="margin-top: 20px;">
                    <div class="card">
                        <div class="card-header">Seleccione las Tallas</div>
                        <div class="card-body">
                            <select id='pre-selected-options2' class="form-control" name="tallas[]" multiple='multiple'>
                                @foreach ($tallas as $talla)
                                    <option value="{{ $talla->id }}">{{ $talla->talla }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 mt-3" style="margin-top: 20px; margin-bottom: 20px">
                    <div class="card">
                        <div class="card-header">Seleccione los Materiales</div>
                        <div class="card-body">
                            <select id='pre-selected-options3' class="form-control" name="materiales[]"
                                multiple='multiple'>
                                @foreach ($materiales as $material)
                                    <option value="{{ $material->id }}">{{ $material->material }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mt-3" style="margin-top: 20px; margin-bottom: 20px  ">
                    <div class="card">
                        <div class="card-header">Seleccione las Fotos</div>
                        <div class="card-body">
                            <input type="file" name="images[]" id="image" multiple class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <input type="submit" class="form-control btn btn-success" value="Guardar" id="botonguardar">
                </div>
            </form>

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });
        $('#pre-selected-options').multiSelect();
        $('#pre-selected-options2').multiSelect();
        $('#pre-selected-options3').multiSelect();
    </script>
@endsection
