<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Administrador</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('admin/css/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{ asset('admin/css/timeline.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('admin/css/startmin.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('admin/css/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{{ asset('admin/css/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('admin/css/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/multi-select.css') }}" rel="stylesheet">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('welcome') }}" target="_blank">Panel de Control</a>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>



            <!-- Top Navigation: Right Menu -->
            <ul class="nav navbar-right navbar-top-links">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu dropdown-user">

                        <li class="divider"></li>
                        <li><a href="{{ route('logoutAdmin') }}"><i class="fa fa-sign-out fa-fw"></i> Cerrar
                                Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Sidebar -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    @if(Auth::user()->tipoUsuario == 1)
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ route('indexAdmin') }}" class="active"><i
                                    class="fa fa-user fa-fw"></i>Soy {{ Auth::user()->name }}</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i>Pedidos<span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>

                                    <a href="{{ route('pedidos') }}">Pedidos</a>
                                    <a href="{{ route('pedidosPA') }}">Por Aprobar</a>
                                   
                                </li>
                            </ul>
                        </li>


                        <li>
                         <a href="{{ route('descuento') }}"><i class="fa fa-percent"></i> Porcentaje de descuento</a>
                        </li>
                        <li>
                            <a href="{{ route('cupones') }}" class="active"><i
                                    class="fa fa-archive fa-fw"></i> Cupones</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i>Configuraciones<span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('tallas') }}">Tallas</a>
                                    <a href="{{ route('colores') }}">Colores</a>
                                    <a href="{{ route('materiales') }}">Materiales</a>

                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ route('productos') }}" class="active"><i
                                    class="fa fa-archive fa-fw"></i> Productos</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i>Usuarios<span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('usuarios', 2) }}">Clientes</a>
                                    <a href="{{ route('usuarios', 1) }}">Administradores</a>
                                    <a href="{{ route('usuarios', 3) }}">Data Entry</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                    @endif

                    @if(Auth::user()->tipoUsuario == 3)
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ route('indexAdmin') }}" class="active"><i
                                    class="fa fa-dashboard fa-fw"></i> Soy {{ Auth::user()->name }}</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i>Pedidos de la tienda<span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    
                                    <a href="{{ route('mispedidospp') }}">Pedidos por despachar</a>
                                    <a href="{{ route('mispedidosdes') }}">Mis Pedidos Despachados</a>
                               
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-cogs fa-fw"></i>Mis Pedidos<span
                                    class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('pedidoNuevo') }}">Nuevo Pedido</a>
                                
                                    <a href="{{ route('mispedidosp') }}">Mis Pedidos sin Pagar</a>
                                    <a href="{{ route('mispedidos') }}">Mis Pedidos Pagados</a>
                                    
                                </li>
                            </ul>
                        </li>

                    </ul>
                    @endif
                </div>
            </div>
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('titulo')</h1>
                    </div>
                </div>

                @yield('contenido')

            </div>
        </div>

    </div>

    <!-- jQuery -->
    <script src="{{ asset('admin/js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('admin/js/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('admin/js/startmin.js') }}"></script>

    <!-- DataTables JavaScript -->
    <script src="{{ asset('admin/js/dataTables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/js/dataTables/dataTables.bootstrap.min.js') }}"></script>
    @yield('scripts')

</body>

</html>
