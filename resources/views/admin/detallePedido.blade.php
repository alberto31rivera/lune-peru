@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Detalle Pedido

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block mt-20">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> {{ $message }} </strong>
                </div>
            @endif

            <div class="table-responsive col-md-12 ">
                <div class="col-md-3">
                    @if ($pedido->estado == 1)
                        <span class="badge badge-success">Pagado</span>
                    @elseif($pedido->estado == 2)
                        <span class="badge badge-warning">En Espera de Validacion</span>
                    @elseif($pedido->estado == 3)
                        <span class="badge badge-danger">Rechazado o Denegado</span>
                    @elseif($pedido->estado == 4)
                        <span class="badge badge-primary">Despachado Por: {{$pedido->despachado->name}} <br><br> El: {{$pedido->updated_at}}</span>
                    @elseif($pedido->estado == 5)
                        <span class="badge badge-danger">Dinero Retornado</span>
                    @endif
                </div>
                <div class="col-md-6">

                    @if ($pedido->type_transaction == 2)
                        <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal"> <i
                                class="fa fa-eye"></i> Mostrar Comprobante</button>
                    @endif
                    @if($pedido->tarjeta == null)
                    <button class="btn btn-danger" onclick="storeComprobante()">Agregar Yape / Plin / Transferencia</button>
                    @endif
                    @if ($pedido->estado != 5 && $pedido->type_transaction == 1)
                        <button class="btn btn-danger" data-toggle="modal" data-target="#exampleModal2"> <i
                                class="fa fa-eye"></i> Retornar Dinero / Anular Pedido</button>
                    @endif

                </div>
                <div class="col-md-3">
                    <select name="" onchange="updateestado()" @if (Auth::user()->tipoUsuario == 3 && $pedido->estado == 2) disabled @endif
                        @if($pedido->estado == 4) disabled @endif
                        class="form-control" id="estado">
                        @if (Auth::user()->tipoUsuario == 3 && $pedido->estado == 1)
                            <option value="1" @if ($pedido->estado == 1) selected @endif>PAGADO</option>
                            <option value="4" @if ($pedido->estado == 4) selected @endif>DESPACHADO</option>
                        @else
                            <option value="1" @if ($pedido->estado == 1) selected @endif>PAGADO</option>
                            <option value="2" @if ($pedido->estado == 2) selected @endif>EN ESPERA DE APROBACION
                            </option>
                            <option value="3" @if ($pedido->estado == 3) selected @endif>DENEGADO</option>
                            <option value="4" @if ($pedido->estado == 4) selected @endif>DESPACHADO</option>
                            <option value="4" @if ($pedido->estado == 5) selected @endif>DINERO DEVUELTO</option>
                        @endif

                    </select>
                    <input type="hidden" name="" id="idPedido" value="{{ $pedido->id }}">
                </div>

                <div class="col-md-3">
                    @if ($pedido->dir_envio == 1)
                        <span class="badge badge-success">RECOJO EN TIENDA</span>
                    @elseif($pedido->dir_envio == 2)
                        <span class="badge badge-warning">DELIVERY</span>
                    @endif
                </div>

                <div class="col-md-12 mb-3" style="margin-botton: 20px;">
                    @if ($pedido->type_transaction == 1)
                        Tarjeta: {{ $pedido->tarjeta }} <br>
                        Marca: {{ $pedido->marca_tarjeta }} <br>
                        Mensaje: {{ $pedido->descripcion_tarjeta }} <br>
                        Id del Cargo Culqi: {{ $pedido->cargo_id }} <br><br>
                        @if ($pedido->estado == 5)
                            Id Retorno: {{ $pedido->id_retorno }} <br>
                            Razon: {{ $pedido->razon_retorno }} <br>
                        @endif
                    @endif
                </div>
                <div style="height: 40px"></div>
                <br><br> <br>
                <table class="table table-striped table-bordered table-hover mt-3" style="margin-top: 20px"
                    id="dataTables-example">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Monto</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pedido->relacionpedido as $producto)
                            <tr>
                                <th>{{ $producto->detalle->titulo }}</th>
                                <th>{{ $producto->cantidad }}</th>
                                <th>{{ $producto->detalle->precio * $producto->cantidad }}</th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <h1>Total: {{ $pedido->total }}</h1>
                @if ($pedido->estado == 1 && $pedido->factura == null)
                    <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal3"> <i
                            class="fa fa-file"></i> Agregar Factura</button>
                @elseif($pedido->estado == 4 && $pedido->factura == null)
                <button class="btn btn-success" data-toggle="modal" data-target="#exampleModal3"> <i
                            class="fa fa-file"></i> Agregar Factura</button>
                @elseif($pedido->estado == 2 && $pedido->factura == null)
                    AUN NO SE PUEDE AGREGAR FACTURA
                @else
                    <a href="https://facturas.luneperu.com/{{ $pedido->factura }}" target="blank">
                        <h1>Ya se cargo una factura</h1>
                    </a>
                @endif
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Comprobante de Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="https://yapes.luneperu.com/{{ $pedido->tarjeta }}" width="100%" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    @if ($pedido->estado == 2)
                        <button type="button" class="btn btn-primary" onclick="updatePago(1, {{ $pedido->id }})">Aprobar
                            Pago</button>
                        <button type="button" class="btn btn-danger" onclick="updatePago(3, {{ $pedido->id }})">Denegar
                            Pago</button>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cancelar Pedido Devolver Dinero</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('retornarDinero') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Razon">Razon</label>
                            <select name="razon" id="" class="form-control">
                                <option value="duplicado">Duplicado</option>
                                <option value="fraudulento">Fraude</option>
                                <option value="solicitud_comprador">Solicitud del Cliente</option>
                            </select>
                            <input type="hidden" name="pedido_id" value="{{ $pedido->id }}" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-danger">Anular Pago</button>

                    </div>
                </form>
            </div>
        </div>
    </div>



<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Comprobante</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('storeComprobante') }}" method="POST" accept-charset="UTF-8"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Razon">Comprobante</label>
                            <input type="file" name="factura" id="">
                            <input type="hidden" name="idPedido" value="{{ $pedido->id }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Agregar</button>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Factura</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('storeFactura') }}" method="POST" accept-charset="UTF-8"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Razon">Factura</label>
                            <input type="file" name="factura" id="">
                            <input type="hidden" name="idPedido" value="{{ $pedido->id }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Agregar</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://checkout.culqi.com/js/v3"></script>
    <script>
        function updatePago(estado, idPedido) {
            axios.get("/updateEstadoPago/" + idPedido + "/" + estado).then((response) => {
                console.log(response)
                location.reload()
            })
        }

        function storeComprobante()
        {
            $("#exampleModal4").modal('show')
        }

        function updateestado() {
            idPedido = $("#idPedido").val();
            estado = $("#estado").val();

            axios.get("/updateEstadoPago/" + idPedido + "/" + estado).then((response) => {
                console.log(response)
                location.reload()
            })
        }
    </script>
@endsection
