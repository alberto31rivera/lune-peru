@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Editar Estado del Producto
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block mt-20">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif
            <div class="col-md-12">
                <h4>Eitar STock</h4>
                <form action="{{route('updateStock')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Producto</label>
                        <input type="text" class="form-control" id="color" disabled value="{{$producto->titulo}}" name="codigo">
                    </div>

                    <div class="form-group">
                        <label for="">Precio</label>
                        <input type="text" class="form-control" value="{{$producto->precio}}" name="precio">
                    </div>

                    <div class="form-group">
                        <label for="">Stock</label>
                        <select name="stock" class="form-control" id="">
                            <option value="0">NO PUBLICADO</option>
                            <option value="1">PUBLICADO</option>
                        </select>
                        <input type="hidden" class="form-control" id="color" value="{{$producto->id}}" name="idProducto">
                    </div>

                    <button type="submit" class="btn btn-success form-control"> Guardar </button>
                    <a href="{{route('productos')}}" class="btn btn-warning form-control mt-3" style="margin-top: 20px">Volver</a>
                </form>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu:     "Mostrar  _MENU_  Registros",
                }
            });
        });
    </script>
@endsection

