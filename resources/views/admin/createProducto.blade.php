@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Crear Producto
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">

            <form action="{{ route('storeProducto') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Categorias</label>
                    <select name="categoria" id="categoria" disabled onchange="getSubCategorias()" class="form-control">
                        <option value="">MUJER</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Titulo</label>
                    <input type="text" name="titulo" required class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Marca</label>
                    <input type="text" name="marca" required class="form-control">
                </div>

                <div class="form-group">
                    <label for="">SKU</label>
                    <input type="text" name="sku" required id="sku" class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Precio</label>
                    <input type="text" name="precio" required class="form-control">
                </div>

                <div class="form-group">
                    <label for="">Descripcion</label>
                    <input type="text" name="descripcion" required class="form-control">
                </div>



                    <div class="card mb-5">
                        <div class="card-header">Seleccione las Fotos</div>
                        <div class="card-body">
                            <input type="file" name="images[]" required id="image" multiple class="form-control">
                        </div>
                    </div>


                <div class="form-group">
                    <input type="submit" class="form-control btn btn-success mt-5" style="margin-top: 20px" value="Guardar" id="botonguardar">
                </div>
            </form>

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/jquery.multi-select.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu: "Mostrar  _MENU_  Registros",
                }
            });
        });
        $('#pre-selected-options').multiSelect();
        $('#pre-selected-options2').multiSelect();
        $('#pre-selected-options3').multiSelect();
    </script>
@endsection
