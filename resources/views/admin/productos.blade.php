@extends('admin.layouts.app')

{{-- @section('titulo')
    GESTION DE TALLAS
@endsection --}}

@section('contenido')
    <div class="panel panel-default">
        <div class="panel-heading">
            Listado de Productos

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block mt-20">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong> {{ $message }} </strong>
                                </div>
                            @endif

            <div class="table-responsive col-md-12 ">
                <div class="col-md-9 mb-2"></div>
                <div class="col-md-3 mb-2">
                    <a href="{{route('crearProducto')}}" class="text-right btn btn-sm btn-success mb-3 form-control"> <i class="fa fa-plus-circle"></i>  Agregar Nuevo</a>
                </div>


                <table class="table table-striped table-bordered table-hover mt-3" id="dataTables-example">
                    <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Producto</th>
                            <th>Estado</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($productos as $producto)
                            <tr class="odd gradeX">
                                <td>{{ $producto->sku }}</td>
                                <td>{{ $producto->titulo }}</td>
                                <td>@if($producto->estado == 1)
                                    Publicado
                                    @else
                                    NO Publicado
                                    @endif
                                </td>
                                <td>S/ {{ $producto->precio }}</td>
                                <td width="30px">
                                    <a href="{{route('editarstock', $producto->id)}}"><i class="fa fa-edit text-success"></i></a>
                                    <a href="{{route('variacionesProducto', $producto->id)}}"><i class="fa fa-database text-warning"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
        <!-- /.panel-body -->
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true,
                language: {
                    search: "Buscar",
                    paginate: {
                        first: "Primera",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Ultima"
                    },
                    info: "Mostrando _START_ de _END_ en _TOTAL_ registros",
                    lengthMenu:     "Mostrar  _MENU_  Registros",
                }
            });
        });
    </script>
@endsection
