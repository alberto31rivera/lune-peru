<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $producto->titulo }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    @include('layouts.rels')

    <style>
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
            margin-left: 40%
        }

        .lds-ripple div {
            position: absolute;
            border: 4px solid #000000;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;

        }

        .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
        }

        @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 0;
            }

            4.9% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 0;
            }

            5% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }

            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
        }

    </style>

</head>

<body>

    <div class="wrapper home-3">
        <!--Header Area Start-->
        @include('layouts.header')
        <!--Header Area End-->

        <!--Single Product Area Start-->
        <section class="single-product-area mt-20">
            <div class="container">
                <!--Single Product Info Start-->
                <div class="row single-product-info mb-50">
                    <!--Single Product Image Start-->
                    <div class="col-lg-6 col-md-6">
                        <div class="flexslider">
                            <ul class="slides">
                                @foreach ($imagenes as $imagen)
                                    <li data-thumb="https://imagenes.luneperu.com/{{ $imagen->imagen }}">
                                        <img src="https://imagenes.luneperu.com/{{ $imagen->imagen }}" width="100%"
                                            height="50%" />
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!--Single Product Image End-->
                    <!--Single Product Content Start-->
                    <div class="col-lg-6 col-md-6">
                        <div class="single-product-content">
                            <!--Product Nav Start-->

                            <!--Product Nav End-->
                            <h1 class="product-title">{{ $producto->titulo }}</h1>
                            <!--Product Rating Start-->

                            <!--Product Rating End-->
                            <!--Product Price Start-->
                            <div class="single-product-price">
                                <span class="new-price">S/ {{ $producto->precio }}</span>
                            </div>
                            <!--Product Price End-->
                            <!--Product Description Start-->
                            <div class="product-description">
                                <p>{{ $producto->descripcion }}</p>
                            </div>
                            <!--Product Description End-->
                            <!--Product Quantity Start-->
                            <div class="single-product-quantity">
                                <div class="quantity">
                                    <label>Cantidad</label>
                                    <input class="input-text" value="1" type="number">
                                    <div class="alert alert-danger" role="alert" style="display: none" id="error">
                                        Debe seleccionar, talla, color y material
                                    </div>
                                </div>

                                <div class="quantity" id="colores">
                                    <label>Colores</label>
                                    <select name="color" id="color" onchange="getTallas({{ $producto->id }})">
                                        <option value="">SELECCIONE</option>
                                        @if ($colores != null)
                                            @foreach ($colores as $color)
                                                <option value="{{ $color->idColor }}">
                                                    {{ $color->color_detalle->color }}
                                                </option>
                                            @endforeach
                                        @endif

                                    </select>
                                </div>

                                <div class="quantity" id="tallas" style="display: none">
                                    <label>Tallas</label>
                                    <select name="talla" id="talla" onchange="getMateriales({{ $producto->id }})">
                                        <option value="">SELECCIONE</option>
                                    </select>
                                </div>

                                <div class="quantity" id="materiales" style="display: none">
                                    <label>Material</label>
                                    <select name="material" id="material">
                                        <option value="">SELECCIONE</option>
                                    </select>
                                </div>
                                <div class="lds-ripple" style="display: none" id="loader">
                                    <div></div>
                                    <div></div>
                                </div>
                                <button class="quantity-button" type="button"
                                    onclick="validar({{ $producto->id }})">Agregar al Carrito</button>

                                <a href="/carrito" class="quantity-button" style="display: none; margin-top: 30px" type="button"
                                    id="carrito">Pagar</a>


                            </div>
                            <!--Product Quantity End-->
                            <!--Product Meta Start-->
                            <div class="product-meta">
                                <span class="posted-in">
                                    Categorías:
                                    <a href="#">Mujer</a>
                                </span>
                            </div>
                            <!--Product Meta End-->
                        </div>
                    </div>
                    <!--Single Product Content End-->
                </div>
                <!--Single Product Info End-->

            </div>
        </section>
        <!--Single Product Area End-->

        <!--Footer Area Start-->
        @include('layouts.footer')
        <!--Footer Area End-->
    </div>



    <!--All Js Here-->

    <!--Jquery 3.6.0-->
    <script src="{{ asset('js/vendor/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-migrate-3.3.2.min.js') }}"></script>
    <!--Popper-->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!--Bootstrap-->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!--Imagesloaded-->
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
    <!--Isotope-->
    <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
    <!--Ui js-->
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <!--Countdown-->
    <script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
    <!--Counterup-->
    <script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
    <!--ScrollUp-->
    <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
    <!--Chosen js-->
    <script src="{{ asset('js/chosen.jquery.js') }}"></script>
    <!--Meanmenu js-->
    <script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
    <!--Instafeed-->
    <script src="{{ asset('js/instafeed.min.js') }}"></script>
    <!--EasyZoom-->
    <script src="{{ asset('js/easyzoom.min.js') }}"></script>
    <!--Fancybox-->
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}"></script>
    <!--Nivo Slider-->
    <script src="{{ asset('js/jquery.nivo.slider.js') }}"></script>
    <!--Waypoints-->
    <script src="{{ asset('js/waypoints.min.js') }}"></script>
    <!--Carousel-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!--Slick-->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!--Wow-->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!--Plugins-->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!--Main Js-->
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('jquery.flexslider.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });

        function validar(idProducto) {
            if ($("#color").val() && $("#talla").val() && $("#material").val()) {
                $("#error").css("display", "none");
                $.ajax({
                    url: '/comprar/' + idProducto + '/' + $("#material").val(),
                    type: 'GET',
                    success: function(response) {

                        $("#carrito").css("display", "block");
                        // location.reload();
                    }
                });
            } else {
                $("#error").css("display", "block");
            }
        }

        function getTallas(idProducto) {
            $("#loader").show()
            $("#tallas").hide()
            $("#materiales").hide()
            color = $("#color").val();
            axios.get('/getTallas/' + idProducto + "/" + color).then((response) => {
                $("#talla").html(response.data.html);
                $("#loader").hide()
                $("#tallas").show()
            });
        }

        function getMateriales(idProducto) {
            $("#loader").show()
            color = $("#color").val()
            talla = $("#talla").val()
            $("#materiales").hide()
            axios.get('/getMateriales/' + idProducto + '/' + color + '/' + talla).then((response) => {
                $("#material").html(response.data.html)
                $("#loader").hide()
                $("#materiales").show()
            })
        }
    </script>
</body>

</html>
