<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cliente')->nulablle();
            $table->string('ip_pedido')->nullable();
            $table->integer('dir_envio')->nullable();
            $table->integer('met_pago')->nullable();
            $table->integer('met_envio')->nullable();
            $table->integer('estado');
            $table->decimal('subtotal')->nullable();
            $table->integer('cupon')->nullable();
            $table->integer('total')->nullable();
            $table->string('tracking');
            $table->integer('type_transaction')->nullable();
            $table->string('tarjeta')->nullable();
            $table->string('marca_tarjeta')->nullable();
            $table->string('descripcion_tarjeta')->nullable();
            $table->integer('carrito_cookie')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
