<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelacionPedidoProductosTable extends Migration
{
    public function up()
    {
        Schema::create('relacion_pedido_productos', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pedido');
            $table->integer('id_producto');
            $table->integer('id_color')->nullable();
            $table->integer('id_talla')->nullable();
            $table->integer('id_material')->nullable();
            $table->integer('cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relacion_pedido_productos');
    }
}
