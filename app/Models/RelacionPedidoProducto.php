<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelacionPedidoProducto extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_pedido',
        'id_producto',
        'id_variacion',
        'cantidad',
    ];

    public function detalle()
    {
        return $this->hasOne(Producto::class, 'id', 'id_producto');
    }

    public function variacion()
    {
        return $this->hasOne(Variaciones::class, 'id', 'id_variacion');
    }

    public function imagenes()
    {
        return $this->hasOne(ImagenProducto::class, 'id_producto', 'id_producto');
    }

    public function color_detalle()
    {
        return $this->hasOne(Clores::class, 'id', 'id_color');
    }

    public function talla_detalle()
    {
        return $this->hasOne(Talla::class, 'id', 'id_talla');
    }

    public function material_detalle()
    {
        return $this->hasOne(Materiales::class, 'id', 'id_material');
    }
}
