<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_cliente',
        'ip_pedido',
        'estado',
        'subtotal',
        'total',
        'tracking',
    ];

    public function relacionpedido()
    {
        return $this->hasMany(RelacionPedidoProducto::class, 'id_pedido', 'id');
    }

    public function variacion()
    {
        return $this->hasOne(Variaciones::class, 'id', 'id_variacion');
    }

    public function despachado()
    {
        return $this->hasOne(User::class, 'id', 'despachado_por');
    }

    public function cupon_detalle()
    {
        return $this->hasOne(Cupon::class, 'id', 'cupon');
    }

    public function referido()
    {
        return $this->hasOne(Referido::class, 'id_referido', 'id_cliente');
    }

    public function cliente()
    {
        return $this->hasOne(User::class, 'id', 'id_cliente');
    }

    public function estado_pedido()
    {
        return $this->hasOne(EstadosPedido::class, 'estado', 'estado');
    }

    public function tracking()
    {
        return $this->hasMany(TrackingPedido::class, 'pedido_id', 'id');
    }

    public function dirfact()
    {
        return $this->hasOne(DireccionCliente::class, 'id', 'dir_facturacion');
    }

    public function direnv()
    {
        return $this->hasOne(DireccionCliente::class, 'id', 'dir_envio');
    }
}
