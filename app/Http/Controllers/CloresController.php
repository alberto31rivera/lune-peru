<?php

namespace App\Http\Controllers;

use App\Models\Clores;
use Illuminate\Http\Request;

class CloresController extends Controller
{
    public function index()
    {
        return view('admin.colores', [
            'colores' => Clores::all()
        ]);
    }

    public function create()
    {
        return view('admin.configuraciones.create-color');
    }

    public function store(Request $request)
    {
        Clores::updateOrCreate(
            [
                'color' => $request->color
            ],
            [
                'color' => $request->color
            ]
        );

        return back()->with('success', 'Color Creado / Actualizado con éxito');
    }
}
