<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use App\Models\ImagenProducto;
use App\Models\Materiales;
use App\Models\{Talla, Pedido, Variaciones};
use App\Models\Clores;
use App\Models\ColoresProductos;
use App\Models\{MaterialesProductos, RelacionPedidoProducto};
use App\Models\TallasProductos;
use Illuminate\Http\Request;

use Auth;

class ProductoController extends Controller
{
    public function index()
    {
        $productos = Producto::paginate(100);
        return view('admin.productos', ['productos' => $productos]);
    }

    public function create()
    {
        $colores = Clores::orderBy('color', 'asc')->get();
        $tallas = Talla::all();
        $materiales = Materiales::all();
        return view('admin.createProducto', compact('colores', 'tallas', 'materiales'));
    }


    public function store(Request $request)
    {

        $producto = new Producto();
        $producto->titulo = $request->titulo;
        $producto->precio = $request->precio;
        $producto->marca = $request->marca;
        $producto->sku = $request->sku;
        $producto->descripcion = $request->descripcion;
        $producto->stock = 1;
        $producto->save();



        $files = $request->file('images');

        foreach ($files as $file) {
            ImagenProducto::create([
                'id_producto' => $producto->id,
                'imagen' => $this->upload_global($file, 'productimage'),
            ]);
        }

        return redirect()->route('variacionesProducto', $producto->id);
    }

    public function updateProducto(Request $request)
    {
        $producto = Producto::find($request->idProducto);
        $producto->titulo = $request->titulo;
        $producto->precio = $request->precio;
        $producto->marca = $request->marca;
        $producto->sku = $request->sku;
        $producto->save();

        return back()->with('success', 'Producto Actualizado con exito');
    }


    public function show($idProducto)
    {
        $titulo = str_replace("-", " ", $idProducto);
        $producto = Producto::where('titulo', $titulo)->first();
        $idProducto = $producto->id;
        $colores = Variaciones::where('idProducto', $idProducto)->with('color_detalle')->distinct('idColor')->get();
        $colores = $colores->unique('idColor');

        $imagenes = ImagenProducto::where('id_producto', $idProducto)->get();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $actual = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }



        return view('producto', [
            'producto' => $producto,
            'imagenes' => $imagenes,
            'colores' => $colores,
            'pedido' => $actual,
            'cantidad' => $cantidad
        ]);
    }

    public function tienda()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $productos = Producto::with('imagenes')->paginate(20);



        $pedido = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($pedido->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        return view('tienda', compact('productos', 'pedido', 'cantidad'));
    }



    public function desactivar(Producto $producto)
    {
        $producto->estado = 2;
        $producto->save();

        return back();
    }

    public function activar(Producto $producto)
    {
        $producto->estado = 1;
        $producto->save();

        return back();
    }

    function upload_global($file, $folder)
    {

        $file_type = $file->getClientOriginalExtension();
        $folder = $folder;
        $destinationPath = public_path() . '/uploads/' . $folder;
        $destinationPathThumb = public_path() . '/uploads/' . $folder . 'thumb';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();
        $url = '/uploads/' . $folder . '/' . $filename;

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }

    public function buscar($q)
    {
        $det = Producto::where('titulo', 'like', "%$q%")->with('imagenes')->get();
        $productos = null;

        return view('todo_99', compact('det', 'productos'));
    }

    public function filtrar(Request $request)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);


        $var = explode('-', $request->monto);
        $productos = Producto::where('precio', '>=', $var[0])->where('precio', '<=', $var[1])->get();


        $pedido = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($pedido->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        return view('tienda2', compact('productos', 'pedido', 'cantidad'));
    }

    public function getAdicionalesProducto($idProducto)
    {
        //aqui
        $producto = Producto::find($idProducto);

        $colores = Variaciones::where('idProducto', $idProducto)->with('color_detalle')->distinct('idColor')->get();
        $colores = $colores->unique('idColor');

        $html = "<option value=''>SELECCIONE</option>";
        foreach ($colores as $color) {
            $color = $color->color_detalle;
            $html .= "<option value='$color->id'>$color->color</option>";
        }
        $colores = $html;


        return response()->json([
            'producto' => $producto,
            'colores' => $colores,
        ]);
    }

    public function editarstock($idProducto)
    {
        $producto = Producto::find($idProducto);
        return view('admin.stock', compact('producto'));
    }

    public function updateStock(Request $request)
    {
        $producto = Producto::find($request->idProducto);
        $producto->stock = $request->stock;
        $producto->precio = $request->precio;
        $producto->save();

        return back()->with('success', 'ESTADO Actualizado con exito');
    }

    public function variacionesProducto($idProducto)
    {
        $producto = Producto::find($idProducto);
        $colores = Clores::all();
        $materiales = Materiales::all();
        $tallas = Talla::all();
        return view('admin.variacionesProductos', compact('producto', 'materiales', 'tallas', 'colores'));
    }

    public function storeVariante(Request $request)
    {
        $variante = Variaciones::updateOrCreate([
            'idProducto' => $request->idProducto,
            'idColor' => $request->color,
            'idTalla' => $request->talla,
            'idMaterial' => $request->material,

        ], [
            'stock' => $request->cantidad
        ]);

        $variaciones = Variaciones::where('idProducto', $request->idProducto)
            ->with('color_detalle', 'talla_detalle', 'material_detalle')
            ->get();

        $html = "";
        foreach ($variaciones as $variacion) {
            $html .= "<tr>";
            $html .= "<td>" . $variacion->color_detalle->color . "</td>";
            $html .= "<td>" . $variacion->talla_detalle->talla . "</td>";
            $html .= "<td>" . $variacion->material_detalle->material . "</td>";
            $html .= "<td>" . $variacion->stock . "</td>";
            $html .= "</tr>";
        }

        return response()->json([
            'html' => $html
        ]);
    }

    public function getVariantes($idProducto)
    {
        $variaciones = Variaciones::where('idProducto', $idProducto)
            ->with('color_detalle', 'talla_detalle', 'material_detalle')
            ->get();

        $html = "";
        foreach ($variaciones as $variacion) {
            $html .= "<tr>";
            $html .= "<td>" . $variacion->color_detalle->color . "</td>";
            $html .= "<td>" . $variacion->talla_detalle->talla . "</td>";
            $html .= "<td>" . $variacion->material_detalle->material . "</td>";
            $html .= "<td>" . $variacion->stock . "</td>";
            $html .= "</tr>";
        }

        return response()->json([
            'html' => $html
        ]);
    }

    public function getTallas($idProducto, $idColor)
    {
        $variaciones = Variaciones::where('idProducto', $idProducto)
            ->where('idColor', $idColor)
            ->distinct('idTalla')
            ->with('talla_detalle')
            ->get();

        $variaciones =  $variaciones->unique('idTalla');

        $html = "";
        $html .= "<option value=''>SELECCIONE</option>";
        foreach ($variaciones as $variacion) {
            $html .= "<option value='$variacion->idTalla'>" . $variacion->talla_detalle->talla . "</option>";
        }

        return response()->json([
            'html' => $html
        ]);
    }

    public function getMateriales($idProducto, $idColor, $idTalla)
    {
        $variaciones = Variaciones::where('idProducto', $idProducto)
            ->where('idColor', $idColor)
            ->where('idTalla', $idTalla)
            ->where('stock', '>', 0)
            ->with('material_detalle')
            ->distinct('idMaterial')
            ->get();

        $variaciones =  $variaciones->unique('idMaterial');

        $html = "";
        $html .= "<option value=''>SELECCIONE</option>";
        foreach ($variaciones as $variacion) {
            $html .= "<option value='$variacion->id'>" . $variacion->material_detalle->material . "</option>";
        }

        return response()->json([
            'html' => $html
        ]);
    }

    public function buscador(Request $request)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $pedido = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        $productos = Producto::latest()
            ->where('estado', 1)
            ->where('stock', '>', 0)
            ->where('titulo', 'like', "%$request->q%")
            ->with('imagenes')
            ->get();


        return view('tienda2', compact('productos', 'pedido', 'cantidad'));
    }
}
