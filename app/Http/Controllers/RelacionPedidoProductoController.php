<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\RelacionPedidoProducto;
use Illuminate\Http\Request;
use Auth;


class RelacionPedidoProductoController extends Controller
{
    public function delete($iProducto)
    {
        $rel = RelacionPedidoProducto::find($iProducto);
        $rel->delete();

        return back();
    }

    public function updateCantidad( $iProducto, $cantidad )
    {
        $rel = RelacionPedidoProducto::find($iProducto);
        $rel->cantidad = $cantidad;
        $rel->save();

        return back();
    }
}
