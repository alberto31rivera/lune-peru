<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDataAdicionalPedidoRequest;
use App\Http\Requests\UpdateDataAdicionalPedidoRequest;
use App\Models\DataAdicionalPedido;

class DataAdicionalPedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDataAdicionalPedidoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDataAdicionalPedidoRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DataAdicionalPedido  $dataAdicionalPedido
     * @return \Illuminate\Http\Response
     */
    public function show(DataAdicionalPedido $dataAdicionalPedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataAdicionalPedido  $dataAdicionalPedido
     * @return \Illuminate\Http\Response
     */
    public function edit(DataAdicionalPedido $dataAdicionalPedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDataAdicionalPedidoRequest  $request
     * @param  \App\Models\DataAdicionalPedido  $dataAdicionalPedido
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDataAdicionalPedidoRequest $request, DataAdicionalPedido $dataAdicionalPedido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataAdicionalPedido  $dataAdicionalPedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataAdicionalPedido $dataAdicionalPedido)
    {
        //
    }
}
