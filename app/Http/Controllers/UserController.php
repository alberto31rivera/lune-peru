<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User, Pedido};


use Auth;

class UserController extends Controller
{
    public function index($tipoUsuario)
    {
        $usuarios = User::where('tipoUsuario', $tipoUsuario)->get();

        return view('admin.usuarios', compact('usuarios'));
    }

    public function create()
    {
        return view('admin.createUsuario');
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->nombre;
        $user->email = $request->email;
        $user->tipoUsuario = $request->tipoUsuario;
        $user->password =
        sha1($request->clave);
        $user->provider = "laravel";
        $user->save();

        return back()->with('success', 'Usuario Creado / Actualizado con exito');

    }


    public function storeDataCliente(Request $request)
    {
        $val = User::whereEmail($request->correo)->count();
        if($val > 0)
        {
            $user = User::whereEmail($request->correo)->first();
            $user->idDepartamento = $request->departamento;
            $user->idProvincia = $request->provincia;
            $user->idDistrito = $request->distrito;
            $user->Direccion = $request->direccion;
            $user->Referencia = $request->apartamento;
        }
        else{
            $user = new User();
            $user->name = $request->nombre." ". $request->apellido;
            $user->email = $request->correo;
            $user->tipoUsuario = 2;
            $user->password =
            sha1(12345);
            $user->provider = "laravel";
            $user->idDepartamento = $request->departamento;
            $user->idProvincia = $request->provincia;
            $user->idDistrito = $request->distrito;
            $user->Direccion = $request->direccion;
            $user->Referencia = $request->apartamento;
            $user->save();
        }

        $var = Pedido::where('id_cliente', $user->id)->where('estado', 0)->count();
        if($var > 0)
        {
            $pedido =
            Pedido::where('id_cliente', $user->id)->where('estado', 0)->first();
            $pedido->creado_por = Auth::user()->id;
            $pedido->save();
        }
        else
        {
            $pedido = new Pedido();
            $pedido->id_cliente = $user->id;
            $pedido->estado = 0;
            $pedido->creado_por = Auth::user()->id;
            $pedido->save();
        }


        return response()->json([
            'user' => $user,
            'pedido' => $pedido
        ]);
    }
}
