<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Pedido, Producto, Banner, RelacionPedidoProducto};

use Auth;

class WelcomeController extends Controller
{
    public function index(Request $request)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $actual = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        $productos_det = Producto::latest()
            ->where('estado', 1)
            ->where('stock', '>', 0)
            ->with('imagenes')
            ->take(12)
            ->get();


        return view('welcome', [
            'pedido' => $actual,
            'productos' => $productos_det,
            'cantidad' => $cantidad
        ]);
    }

    public function nosotros()
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);


        $actual = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }
        return view('nosotros', [
            'pedido' => $actual,
            'cantidad' => $cantidad
        ]);
    }

    public function contacto()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);


        $actual = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        return view('contacto', [
            'pedido' => $actual,
            'cantidad' => $cantidad
        ]);
    }

    public function videos()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);


        $actual = Pedido::where('ip_pedido', $result)
            ->where('estado', 0)
            ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
            ->first();
        if (isset($actual->id)) {
            $cantidad = RelacionPedidoProducto::where('id_pedido', $actual->id)->sum('cantidad');
        } else {
            $cantidad = 0;
        }

        return view('videos', [
            'pedido' => $actual,
            'cantidad' => $cantidad
        ]);
    }

}
