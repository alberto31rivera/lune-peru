<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\RelacionPedidoProducto;
use App\Models\PorcentajeDescuento;
use App\Models\{Cupon, Distrito, Deprtamento, Provincia, User, Producto, Variaciones};
use Illuminate\Http\Request;
use Auth;

class CarritoController extends Controller
{
    public function index()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $ip = curl_exec($ch);
        curl_close($ch);

        $vali = Pedido::whereIpPedido($ip)->whereEstado(0)->count();

        if ($vali > 0) {
            $pedido = Pedido::whereIpPedido($ip)
                ->whereEstado(0)
                ->with([
                    'relacionpedido',
                    'relacionpedido.variacion',
                    'relacionpedido.variacion.color_detalle',
                    'relacionpedido.variacion.material_detalle',
                    'relacionpedido.variacion.talla_detalle',
                    'relacionpedido.detalle',
                    'relacionpedido.imagenes',
                    'cupon_detalle'
                ])
                ->first();
            $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
            if ($cantidad > 2) {
                $porcentaje = PorcentajeDescuento::find(1);
                $porcentaje = $porcentaje->porcentaje;
            } else {
                $porcentaje = 0;
            }
        } else {
            return redirect('/');
        }


        return view('carrito', compact('pedido', 'cantidad', 'porcentaje'));
    }

    public function updateChecout($totalPedido, $idPedido, $subTotal)
    {
        $pedido = Pedido::find($idPedido);
        $pedido->total = $totalPedido;
        $pedido->subtotal = $subTotal;
        $pedido->save();

        return 200;
    }

    public function checkout($idPedido)
    {
        $pedido = Pedido::whereId($idPedido)
            ->whereEstado(0)
            ->with([
                'relacionpedido',
                'relacionpedido.detalle',
                'relacionpedido.imagenes',
                'relacionpedido.color_detalle',
                'relacionpedido.talla_detalle',
                'relacionpedido.material_detalle',
                'cupon_detalle'
            ])
            ->first();

        $departamentos = Deprtamento::all();
        $distritos = Distrito::all();
        $provincias = Provincia::all();

        $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');

        return view('chechout', compact('pedido', 'cantidad', 'distritos', 'provincias', 'departamentos'));
    }

    public function pago(Request $request)
    {
        $pedido = Pedido::find($request->idPedido);
        $monto = $pedido->total * 100;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.culqi.com/v2/charges');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"amount\": \"$monto\",\n  \"currency_code\": \"PEN\",\n  \"email\": \"$request->correo\",\n  \"source_id\":\"$request->tokenCulqi\"\n}");

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Authorization: Bearer sk_live_a3e5b11a60d28fe7';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        $user = User::whereEmail($request->correo)->count();
        if ($user > 0) {
            $user = User::whereEmail($request->correo)->first();
        } else {
            $user = new User();
            $user->name = $request->name . " " . $request->apellido;
            $user->email = $request->correo;
            $user->password = sha1(12345);
            $user->provider = "laravel";
            $user->save();
        }

        if (isset($result['outcome']['type']) && $result['outcome']['type'] == "venta_exitosa") {
            $pedido->type_transaction = 1;
            $pedido->tarjeta = $result['source']['card_number'];
            $pedido->marca_tarjeta = $result['source']['iin']['card_brand'];
            $pedido->descripcion_tarjeta = $result['outcome']['merchant_message'];
            $pedido->cargo_id = $result['id'];
            $pedido->estado = 1;
            $pedido->dir_envio = $request->tipoEnvio;
            $pedido->id_cliente = $user->id;
            $pedido->save();


            Auth::loginUsingId($user->id);

            $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


            foreach ($productos as $producto) {
                $variacion =  Variaciones::find($producto->id_variacion);
                $variacion->stock = $variacion->stock - $producto->cantidad;
                $variacion->save();
            }

            return "Venta Exitosa";
        } else {
            return "Venta Fallida";
        }

        return $result;
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    public function aplicarCupon(Request $request)
    {
        $pedido = Pedido::find($request->idPedido);
        $cupon = Cupon::whereCodigo($request->cupon)->first();


        if ($cupon != null) {
            $pedido->cupon = $cupon->id;
            $pedido->save();
            return back()->with('success', 'Cupon Aplicado con exito');
        } else {
            return back()->with('danger', 'Este cupon no existe');
        }
    }

    public function pagoYape(Request $request)
    {

        $pedido = Pedido::find($request->idPedido);
        $user = User::whereEmail($request->correo)->count();
        if ($user > 0) {
            $file = $request->file('comprobante');
            $user = User::whereEmail($request->correo)->first();
            $pedido->id_cliente = $user->id;
            $pedido->estado = 2;
            $pedido->tarjeta = $this->upload_global($file, 'yapepagos');
            $pedido->type_transaction = 2;
            $pedido->dir_envio = $request->tipoEnvio;
            $pedido->save();

            $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


            foreach ($productos as $producto) {
                $variacion =  Variaciones::find($producto->id_variacion);
                $variacion->stock = $variacion->stock - $producto->cantidad;
                $variacion->save();
            }

            Auth::loginUsingId($user->id);
            return redirect()->route('micuenta');
        } else {
            $user = new User();
            $user->name = $request->name . " " . $request->apellido;
            $user->email = $request->correo;
            $user->password = sha1(12345);
            $user->provider = "laravel";
            $user->save();

            $file = $request->file('comprobante');
            $pedido->id_cliente = $user->id;
            $pedido->estado = 2;
            $pedido->tarjeta = $this->upload_global($file, 'yapepagos');
            $pedido->type_transaction = 2;
            $pedido->save();


            $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


            foreach ($productos as $producto) {
                $variacion =  Variaciones::find($producto->id_variacion);
                $variacion->stock = $variacion->stock + $producto->cantidad;
                $variacion->save();
            }

            Auth::loginUsingId($user->id);
            return redirect()->route('micuenta');
        }
    }

    public function retornarDinero(Request $request)
    {
        $razon = $request->razon;
        $pedido = Pedido::find($request->pedido_id);
        $monto = $pedido->total * 100;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.culqi.com/v2/refunds');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"amount\": \"$monto\",\n       \"charge_id\": \"$pedido->cargo_id\",\n       \"reason\": \"$razon\"}");

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer sk_test_6jeZmv0L9qr2QpTE';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result, true);

        $pedido->estado = 5;
        $pedido->id_retorno = $result['id'];
        $pedido->razon_retorno = $result['reason'];
        $pedido->save();

        $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


        $productos = RelacionPedidoProducto::where('id_pedido', $pedido->id)->get();


        foreach ($productos as $producto) {
            $variacion =  Variaciones::find($producto->id_variacion);
            $variacion->stock = $variacion->stock + $producto->cantidad;
            $variacion->save();
        }



        return back();
    }

    function upload_global($file, $folder)
    {

        $file_type = $file->getClientOriginalExtension();
        $folder = $folder;
        $destinationPath = public_path() . '/uploads/' . $folder;
        $destinationPathThumb = public_path() . '/uploads/' . $folder . 'thumb';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();
        $url = '/uploads/' . $folder . '/' . $filename;

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }

    public function factura(Request $request)
    {
        $file = $request->file('factura');
        $pedido = Pedido::find($request->idPedido);
        $pedido->factura = $this->upload_global($file, 'facturas');
        $pedido->save();

        return back();
    }

    public function comprobante(Request $request)
    {
        $file = $request->file('factura');
        $pedido = Pedido::find($request->idPedido);
        $pedido->tarjeta = $this->upload_global($file, 'yapepagos');
        $pedido->type_transaction = 2;
        $pedido->save();


        return back();
    }
}
