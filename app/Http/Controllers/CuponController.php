<?php

namespace App\Http\Controllers;

use App\Models\Cupon;
use Illuminate\Http\Request;

class CuponController extends Controller
{
    public function index()
    {
        return view('admin.cupones', [
            'cupones' => Cupon::all()
        ]);
    }


    public function store(Request $request)
    {
        Cupon::updateOrCreate(
            [
                'codigo' => $request->codigo
            ],
            [
                'tipo' => $request->tipo,
                'aplica' => $request->cantidad
            ]
        );

        return back()->with('success', 'Cupon Creado / Actualizado con exito');
    }
}
