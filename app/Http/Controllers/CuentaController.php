<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\RelacionPedidoProducto;
use App\Models\PorcentajeDescuento;
use App\Models\{Cupon, Distrito, Deprtamento, Provincia, User};
use Illuminate\Http\Request;
use Auth;

class CuentaController extends Controller
{
    public function index()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);



        if (isset(Auth::user()->id)) {
                $pedidos = Pedido::where('id_cliente', Auth::user()->id)->where('estado', '>', 0)->get();
            $pedido = Pedido::where('ip_pedido', $result)
                ->where('estado', 0)
                ->with(['relacionpedido', 'relacionpedido.detalle', 'relacionpedido.color_detalle', 'relacionpedido.talla_detalle', 'relacionpedido.imagenes'])
                ->first();
            if (isset($pedido->id)) {
                $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
            } else {
                $cantidad = 0;
            }
            return view('account.mis_pedidos', compact('pedidos', 'pedido', 'cantidad'));
        }
        else
        {
            return redirect()->route('login_post');
        }
    }

    public function detalle_pedido($idPedido)
    {
        $pedido = Pedido::where('id', $idPedido)
            ->with([
                'relacionpedido',
                'relacionpedido.variacion',
                'relacionpedido.variacion.color_detalle',
                'relacionpedido.variacion.material_detalle',
                'relacionpedido.variacion.talla_detalle',
                'relacionpedido.detalle',
                'relacionpedido.imagenes',
                'cupon_detalle',
                'despachado'
            ])
            ->first();




        $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
        if ($cantidad > 2) {
            $porcentaje = PorcentajeDescuento::find(1);
            $porcentaje = $porcentaje->porcentaje;
        } else {
            $porcentaje = 0;
        }


        return view('account.detalles_pedido', compact('pedido', 'cantidad', 'porcentaje'));
    }

    public function cambiaClaveV()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.ipify.org');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $pedido = Pedido::where('ip_pedido', $result)
            ->with([
                'relacionpedido',
                'relacionpedido.detalle',
                'relacionpedido.imagenes',
                'relacionpedido.color_detalle',
                'relacionpedido.talla_detalle',
                'relacionpedido.material_detalle',
                'cupon_detalle'
            ])
            ->first();


        $cantidad = RelacionPedidoProducto::where('id_pedido', $pedido->id)->sum('cantidad');
        if ($cantidad > 2) {
            $porcentaje = PorcentajeDescuento::find(1);
            $porcentaje = $porcentaje->porcentaje;
        } else {
            $porcentaje = 0;
        }
        return view('account.cambiarclave', compact('pedido', 'cantidad', 'porcentaje'));
    }

    public function updateClave(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->password = sha1($request->clave);
        $user->save();

        return back()->with('success', 'Clave Cambiada con exito');
    }
}
