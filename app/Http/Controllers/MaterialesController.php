<?php

namespace App\Http\Controllers;

use App\Models\Materiales;
use Illuminate\Http\Request;

class MaterialesController extends Controller
{

    public function index()
    {
        return view('admin.materiales', [
            'materiales' => Materiales::all()
        ]);
    }


    public function store(Request $request)
    {
        Materiales::updateOrCreate(
            [
                'material' => $request->material
            ],
            [
                'material' => $request->material
            ]
        );

        return back()->with('success', 'Material Creado / Actualizado con exito');
    }
}
